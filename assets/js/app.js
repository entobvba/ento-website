import Vue from 'vue';
import Modernizr from 'modernizr';
import Example from './components/Example';
import Navigation from './components/Navigation';


/**
* Create a fresh Vue Application instance
*/
new Vue({
  el: '#app',
  components: {Example, Navigation}
});

const $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');
