<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'webpack_encore.manifest_lookup' shared service.

include_once $this->targetDirs[3].'/vendor/knpuniversity/webpack-encore-bundle/src/Asset/ManifestLookup.php';

return $this->privates['webpack_encore.manifest_lookup'] = new \KnpUniversity\WebpackEncoreBundle\Asset\ManifestLookup(($this->targetDirs[3].'/public/build/manifest.json'));
