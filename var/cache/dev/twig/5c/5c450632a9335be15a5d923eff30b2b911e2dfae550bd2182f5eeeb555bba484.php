<?php

/* default/index.html.twig */
class __TwigTemplate_257e50ae4640b3ad9b57fb765e27d422775ff8e4262eb8ed975c08d4b35d30e7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<navigation></navigation>

<section class=\"intro\">
  <div class=\"intro-slider\">
    <div class=\"intro-slider--item\">
      <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ento_intro-placeholder.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" />
    </div>
    <div class=\"intro-slider--item\">
      <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ento-placeholder.jpg"), "html", null, true);
        echo "\" class=\"img-fluid\" />
    </div>
  </div>
</section>

<section class=\"container-fluid\">
  <div class=\"row\">
    <div class=\"col-6\">
      <h2>
        <b>Immovatief</b>
          in vastgoed <br />
          communicatie
          <span>online</span>
      </h2>
    </div>
    <div class=\"col-6\">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed placerat nulla.
        Integer ac ultrices enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec aliquam posuere tortor eu lacinia.
      </p>
      <a>
        <i class=\"fal fa-long-arrow-right\"></i>
      </a>
    </div>
  </div>
</section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  60 => 9,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<navigation></navigation>

<section class=\"intro\">
  <div class=\"intro-slider\">
    <div class=\"intro-slider--item\">
      <img src=\"{{ asset('build/images/ento_intro-placeholder.jpg') }}\" class=\"img-fluid\" />
    </div>
    <div class=\"intro-slider--item\">
      <img src=\"{{ asset('build/images/ento-placeholder.jpg') }}\" class=\"img-fluid\" />
    </div>
  </div>
</section>

<section class=\"container-fluid\">
  <div class=\"row\">
    <div class=\"col-6\">
      <h2>
        <b>Immovatief</b>
          in vastgoed <br />
          communicatie
          <span>online</span>
      </h2>
    </div>
    <div class=\"col-6\">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed placerat nulla.
        Integer ac ultrices enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec aliquam posuere tortor eu lacinia.
      </p>
      <a>
        <i class=\"fal fa-long-arrow-right\"></i>
      </a>
    </div>
  </div>
</section>

{% endblock %}
", "default/index.html.twig", "/Users/jeroendegroof/Documents/Development/entowebsite/templates/default/index.html.twig");
    }
}
