const path = require('path');
const Encore = require('@symfony/webpack-encore');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const devMode = process.env.NODE_ENV !== 'production';
const glob = require("glob");

Encore
  // the project directory where compiled assets will be stored
  .setOutputPath('public/build/')
  // the public path used by the web server to access the previous directory
  .setPublicPath('/build')
  .enableSourceMaps(!Encore.isProduction())
  .enableSassLoader()
  .enablePostCssLoader((options) => {
    options.config = {
      path: 'config/postcss.config.js'
    };
  })
  .autoProvideVariables({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery',
  })
  .enableVueLoader()
  .enableVersioning()
  .cleanupOutputBeforeBuild()
  .addEntry('js/app', './assets/js/app.js')
  .addStyleEntry('css/app', ['./assets/scss/app.scss'])
  .addEntry('img', glob.sync('./assets/img/*'))
  .splitEntryChunks()
  .enableSingleRuntimeChunk();

module.exports = {
  module: {
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: true // set to true if you want JS source maps
        })
      ]
    },
    rules: [{
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['env'],
          },
        }, {
          loader: 'eslint-loader',
          options: {
            failOnError: true,
          },
        }]
      },
      {
        test: /\.scss$/,
        use: [{
            loader: MiniCssExtractPlugin.loader,
          },
          'style-loader',
          "css-loader",
          'postcss-loader',
          'sass-loader',
          OptimizeCssAssetsPlugin.loader
        ]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'url-loader',
            options: {
              limit: 8000
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 6
              },
              optipng: {
                enabled: true,
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              },
              // the webp option will enable WEBP
              webp: {
                quality: 75
              }
            }
          }
        ]
      },
      {
        type: 'javascript/auto',
        test: /\.modernizrrc$/,
        use: ['modernizr-loader', 'json-loader'],
      },
    ],
    plugins: [
      new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[id].css"
      }),
      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.css$/,
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {
          safe: true,
          discardComments: {
            removeAll: true
          }
        },
        canPrint: true
      })
    ]
  },
  resolve: {
    alias: {
       modernizr$: path.resolve(__dirname, '.modernizrrc'),
    }
  },
  stats: {
    colors: true,
  }
};

module.exports = Encore.getWebpackConfig();
